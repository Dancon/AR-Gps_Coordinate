﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestTextScript : MonoBehaviour {

	// Use this for initialization
	public Text text,
				text1,
				text2,
				text3,
				text4,
				text5,
				text6,
				text7;
				public Image im;

	void Start () {
		Input.compass.enabled = true;
	}
	
	// Update is called once per frame
	void Update () {
		float a1 = TestGpsScript.gyro.attitude.z * 180 / Mathf.PI;
		float a2 = TestGpsScript.gyro.attitude.eulerAngles.z;
			text.text = "Latitude: " + TestGpsScript.Instance.latitude.ToString(); 
			text1.text = "Longitude: " + TestGpsScript.Instance.longitude.ToString();
			text2.text = TestGpsScript.status;
			text3.text = "Next lat: " + TestGpsScript.lat2.ToString();
			text4.text = "Next lon: " + TestGpsScript.long2.ToString();
			text5.text = "Distance: " + TestGpsScript.distance.ToString();
			text6.text = TestGpsScript.gyro.attitude.eulerAngles.ToString() + "   " + a2.ToString();
			text7.text = "Azimut: " + TestGpsScript.angle.ToString();

		
	}
}
