﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestGpsScript : MonoBehaviour {

	// Use this for initialization

	public static TestGpsScript Instance{set; get;}

	public float latitude;
	public float longitude;

	public static string status;
	public static float lat2 = 53.1575f, long2 = 26.004778f;

	public static float distance, angle;

	public static Gyroscope gyro;
	void Start () {
			gyro = Input.gyro;
			gyro.enabled = true;
			gyro.updateInterval = 0.1f;
			Debug.Log(gyro.attitude.ToString());
			Debug.Log(gyro.attitude.eulerAngles.ToString());
			Input.compass.enabled = true;
			Instance = this;
			DontDestroyOnLoad(gameObject);
			StartCoroutine(Gps());
	}
	
	// Update is called once per frame
	void Update () {
	}

	private IEnumerator Gps(){
		// latitude = 53.15614f;
		// longitude = 26.01116f;

		
		if(!Input.location.isEnabledByUser){
			status = "Gps didn't load";
			Debug.Log("Gps didn't load");
			yield break;
		}
		Input.location.Start(1f,1f);

		int waitingTime = 20;

		while(Input.location.status == LocationServiceStatus.Initializing && waitingTime > 0){
			  status = "Waiting";
			  yield return new WaitForSeconds(1);
			  waitingTime--;
		}
		if(waitingTime <= 0){
			status = "Timeout";
			Debug.Log("Timeout");
			yield break;
		}
		if(Input.location.status == LocationServiceStatus.Failed){
			status = "User denied access to location service";
			Debug.Log("User denied access to location service");
			yield break;
		}

		status = "OK";
		latitude = Input.location.lastData.latitude;
		longitude = Input.location.lastData.longitude;
		
		distance = CalculateDistance(latitude, lat2, longitude, long2, distance);
		angle = CalculateAngle(latitude, lat2, longitude, long2, distance, angle);
		Debug.Log(distance.ToString() + "   " + angle.ToString());

		StartCoroutine(Gps());
		yield break;
	}
	public float CalculateDistance(float lat1, float lat2, float long1, float long2, float distance){
		// float lat1 = 53.15607f;
		// float lat2 = 53.157452f;
		// float long1 = 26.01092f;
		// float long2 = 26.010368f;
		const float radius = 6378.137f;
		//float distance;
		//calculate distance
		var dLat = lat2 * Mathf.PI/180 - lat1 * Mathf.PI/180;
		var dLon = long2 * Mathf.PI/180 - long1 * Mathf.PI/180;
		float a = Mathf.Sin(dLat/2) * Mathf.Sin(dLat/2) + 
			Mathf.Cos(lat1 * Mathf.PI/180) * Mathf.Cos(lat2 * Mathf.PI/180) * 
			Mathf.Sin(dLon/2) * Mathf.Sin(dLon/2);
		var c = 2 * Mathf.Atan2(Mathf.Sqrt(a), Mathf.Sqrt(1-a));
		distance = radius * c * 1000f;
		Debug.Log("Distance: " + distance.ToString());
		return distance;
		//calculate alpha
		
	}

	public float CalculateAngle(float lat1, float lat2, float long1, float long2, float distance, float alpha){
		const float radius = 6378.137f;
		
		var dLon = long2 * Mathf.PI/180 - long1 * Mathf.PI/180;

		var dLat = 0;
		float a = Mathf.Sin(dLat/2) * Mathf.Sin(dLat/2) + 
			Mathf.Cos(lat1 * Mathf.PI/180) * Mathf.Cos(lat2 * Mathf.PI/180) * 
			Mathf.Sin(dLon/2) * Mathf.Sin(dLon/2);
		var c = 2 * Mathf.Atan2(Mathf.Sqrt(a), Mathf.Sqrt(1-a));
		float x = c*1000f*radius;
		float z = x/distance;
			//var alpha = z*180/Mathf.PI;
			Debug.Log(z.ToString());
		alpha = Mathf.Asin(z) * 180/Mathf.PI;
		Debug.Log("Alpha: " + alpha.ToString());
		return alpha;
	}
}
